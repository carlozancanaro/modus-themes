# Modus themes (Modus Operandi and Modus Vivendi)

A pair of highly accessible themes that conform with the WCAG AAA
standard for colour contrast between background and foreground
combinations (a minimum contrast of 7:1---the highest standard of its
kind).

The themes are built into GNU Emacs 28.  They are also distributed in
several packages formats.

+ `modus-operandi` is light
+ `modus-vivendi` is dark

## Further information

Read the [Info manual HTML](https://protesilaos.com/modus-themes)
version for how to install, load, enable, and customise the themes.

If you are using the latest version of the themes, you already have the
manual installed: evaluate `(info "(modus-themes) Top")` to start
reading it.

The themes cover a broad range of packages and are highly customisable.

For some demo content, check:

+ the screenshots https://protesilaos.com/modus-themes-pictures/
+ my videos https://protesilaos.com/code-casts/
